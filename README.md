<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

  - [About This List](#about-this-list)
  - [Principles](#principles)
- [Essentials](#essentials)
  - [Books](#books)
  - [Team Dynamics](#team-dynamics)
  - [Certifications](#certifications)
- [Topics](#topics)
  - [Stakeholder Management](#stakeholder-management)
  - [Artificial Intelligence (AI)](#artificial-intelligence-ai)
  - [Change Management](#change-management)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## About This List

A collection of resources for IT auditors.

This page aims to enhance your skills as an auditor by providing resources that I have personally found useful or have stood the test of time in the field.

## Principles

- This page is not meant to be comprehensive.
- The selection of articles is opinionated.

Items:

- 🧰 List of resources
- 📖 Book
- 📺 Video
- ⭐️ Must read
- 📃 Article

# Essentials

## Books

- 📖 [IT Auditing Using Controls to Protect Information Assets](https://www.oreilly.com/library/view/it-auditing-using/9781260453232/)
- 📖 ⭐️ [Beyond Agile Auditing](https://itrevolution.com/product/beyond-agile-auditing/)
- 📖 [Investments Unlimited](https://itrevolution.com/product/investments-unlimited/)
- 📖 [The Delicate Art of Bureaucracy](https://itrevolution.com/product/the-delicate-art-of-bureaucracy/)
- 📖 [The Phoenix Project](https://itrevolution.com/product/the-phoenix-project/)

## Team Dynamics

- 📃 [40 Lessons in 40 Years](https://schlaf.medium.com/40-lessons-from-40-years-de39d2c622d6)
- 📃 [Fixed vs. Growth: The Two Basic Mindsets That Shape Our Lives](https://www.themarginalian.org/2014/01/29/carol-dweck-mindset/)
- 📃 [Steve Jobs: if you don't ask for help, you won't get very far](https://www.youtube.com/watch?v=zkTf0LmDqKI&ab_channel=SiliconValleyHistoricalAssociation)
- 📃 [Be Kind](https://boz.com/articles/be-kind)

## Certifications

- 📃 [IT Certifications Map](https://gitlab.com/nutellabutter/it-audit/-/raw/main/articles/IT_Certifications.png?inline=false)

# Topics

## Stakeholder Management

- 📃 [To Rate or Not to Rate](https://gitlab.com/nutellabutter/it-audit/-/raw/main/articles/ey-to-rate-or-not-to-rate.pdf?ref_type=heads&inline=false)
- 📃 [Put Away Your Radar Huns and Focus on Traffic Safety](https://www.richardchambers.com/internal-auditors-put-away-your-radar-guns-and-focus-on-traffic-safety/)
- 📃 [Building a Better Auditor: Empathy in Audit](https://internalauditor.theiia.org/en/voices/2024/february/building-a-better-auditor-empathy-in-audit/)

## Artificial Intelligence (AI)

- [Data Scientists Targeted by Malicious Hugging Face ML Models with Silent Backdoor](https://jfrog.com/blog/data-scientists-targeted-by-malicious-hugging-face-ml-models-with-silent-backdoor)

## Change Management

- 📃 [McDonald's: Global outage was caused by "configuration change"](https://www.bleepingcomputer.com/news/technology/mcdonalds-global